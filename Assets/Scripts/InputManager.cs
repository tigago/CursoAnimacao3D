﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    #region Input Definitions
    public InputAction MoveCharacter, Jump, Speak;
    //For every new input added above, "on enabled" and "on disabled" must be incremented below
    private void OnEnable()
    {
        MoveCharacter.Enable();
        Jump.Enable();
        Speak.Enable();
    }
    private void OnDisable()
    {
        MoveCharacter.Disable();
        Jump.Disable();
        Speak.Disable();
    }
    #endregion

    private void Awake()
    {
        Instance = this;
    }
}
