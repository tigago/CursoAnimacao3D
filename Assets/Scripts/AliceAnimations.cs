using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliceAnimations : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private CharacterController _charController;
    [SerializeField] private PlayerMovementScript _playerMovement;
    [SerializeField] private Transform _cameraTransform;
    private Vector3 _lastPosition;

    private float _leanWeight //entre -1 e 1
    {
        set
        {
            if (value >= 0)
            {
                _animator.SetLayerWeight(_animator.GetLayerIndex("LeanRightLayer"), value);
                _animator.SetLayerWeight(_animator.GetLayerIndex("LeanLeftLayer"), 0f);
            }
            else
            {
                _animator.SetLayerWeight(_animator.GetLayerIndex("LeanRightLayer"), 0f);
                _animator.SetLayerWeight(_animator.GetLayerIndex("LeanLeftLayer"), Mathf.Abs(value));
            }
        }

        get
        {
            return _animator.GetLayerWeight(_animator.GetLayerIndex("LeanRightLayer")) - _animator.GetLayerWeight(_animator.GetLayerIndex("LeanLeftLayer"));
        }
    }

    private float _twistWeight //entre -1 e 1
    {
        set
        {
            if (value >= 0)
            {
                _animator.SetLayerWeight(_animator.GetLayerIndex("TwistRightLayer"), value);
                _animator.SetLayerWeight(_animator.GetLayerIndex("TwistLeftLayer"), 0f);
            }
            else
            {
                _animator.SetLayerWeight(_animator.GetLayerIndex("TwistRightLayer"), 0f);
                _animator.SetLayerWeight(_animator.GetLayerIndex("TwistLeftLayer"), Mathf.Abs(value));
            }
        }

        get
        {
            return _animator.GetLayerWeight(_animator.GetLayerIndex("TwistRightLayer")) - _animator.GetLayerWeight(_animator.GetLayerIndex("TwistLeftLayer"));
        }
    }

    private void Start()
    {
        _lastPosition = transform.position;
        InputManager.Instance.Speak.started += StartedSpeaking;
        InputManager.Instance.Speak.canceled += CanceledSpeaking;
    }

    private void CanceledSpeaking(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        _animator.SetLayerWeight(_animator.GetLayerIndex("SpeakingLayer"), 0f);
    }

    private void StartedSpeaking(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        _animator.SetLayerWeight(_animator.GetLayerIndex("SpeakingLayer"), 1f);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 deltaPosition = transform.position - _lastPosition;
        Vector3 speed = deltaPosition / Time.deltaTime;
        float horizontalSpeed = new Vector3(speed.x, 0f, speed.z).magnitude / _playerMovement.MoveSpeed;
        float verticalSpeed = speed.y;
        _lastPosition = transform.position;
        _animator.SetFloat("SpeedHorizontal", Mathf.Lerp(_animator.GetFloat("SpeedHorizontal"),horizontalSpeed, 10f * Time.deltaTime));
        _animator.SetFloat("SpeedVertical", verticalSpeed);
        _animator.SetBool("Jumping", !_charController.isGrounded) ;

        //Deixa o movimento da Alice mais fluido
        Vector3 moveInput = new Vector3(InputManager.Instance.MoveCharacter.ReadValue<Vector2>().x, 0f, InputManager.Instance.MoveCharacter.ReadValue<Vector2>().y); //transforma o input 2D em um vetor 3D
        moveInput = Quaternion.AngleAxis(_cameraTransform.eulerAngles.y, Vector3.up) * moveInput; //rotaciona o input para se adequar a rota��o da c�mera
        float angle = moveInput.magnitude == 0f? 0f : Vector3.SignedAngle(transform.forward, moveInput, Vector3.up);
        float finalWeight = Mathf.Clamp(angle / 90f, -1f, 1f);
        float targetLean = finalWeight * horizontalSpeed;
        float targetTwist = finalWeight * (1 - horizontalSpeed);
        _leanWeight = Mathf.Lerp(_leanWeight, targetLean, 10f * Time.deltaTime);
        _twistWeight = Mathf.Lerp(_twistWeight, targetTwist, 10f * Time.deltaTime);

    }
}
