using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliceSoundManager : MonoBehaviour
{
    [SerializeField] private AudioClip[] _stepSounds;
    [SerializeField] private AudioSource _audioSource;

    public void PlayStepSound()
    {
        int index = Random.Range(0, _stepSounds.Length);
        _audioSource.PlayOneShot(_stepSounds[index]);
    }
}
