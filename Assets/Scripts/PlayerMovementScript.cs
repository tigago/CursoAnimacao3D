using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour
{
    public static List<PlayerMovementScript> AllPlayers = new List<PlayerMovementScript>();
    [SerializeField] private CharacterController _charController;
    [SerializeField] private CapsuleCollider _collider;
    [SerializeField] private float _moveSpeed = 5f;
    [SerializeField] private float _jumpForce = 10f;
    [SerializeField] private float _airControl = 10f;
    [SerializeField] private float _groundControl = 15f;
    [SerializeField] private float _gravityScale = 1f;
    [SerializeField] private float _rotationSpeed = 1f;
    [SerializeField] private Transform _cameraTransform;
    public float MoveSpeed { get { return _moveSpeed; } }
    private float _yVel = 0f;
    private bool _jumpTrigger = false;
    private Vector3 _MoveAmount = Vector3.zero;
    private InputManager _playerInput;
    public bool CanMove = true;
    private float _originalSlope;
    private float _currentMoveSpeed;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        AllPlayers.Add(this);
        _currentMoveSpeed = _moveSpeed;
        _originalSlope = _charController.slopeLimit;
        _playerInput = InputManager.Instance;
        _playerInput.Jump.performed += OnJumpPressed;
    }


    private void OnDestroy()
    {
        AllPlayers.Remove(this);
        _playerInput.Jump.performed -= OnJumpPressed;
    }

    private void OnJumpPressed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (!_charController.isGrounded) return;
        _jumpTrigger = true;
    }

    private void Update()
    {
        Vector3 moveInput = new Vector3(_playerInput.MoveCharacter.ReadValue<Vector2>().x, 0f, _playerInput.MoveCharacter.ReadValue<Vector2>().y).normalized;
        moveInput = Quaternion.AngleAxis(_cameraTransform.eulerAngles.y, Vector3.up) *moveInput;
        if (CanMove)
        {
            Vector3 targetMoveAmount = moveInput * _currentMoveSpeed * Time.deltaTime;
            if (!_charController.isGrounded)
            {
                _MoveAmount = Vector3.Lerp(_MoveAmount, targetMoveAmount, _airControl * Time.deltaTime);
            }
            else
            {
                _MoveAmount = Vector3.Lerp(_MoveAmount, targetMoveAmount, _groundControl * Time.deltaTime);
            }
            _charController.Move(_MoveAmount);
        }
        if (_charController.isGrounded && !_jumpTrigger)
        {
            _charController.slopeLimit = _originalSlope;
            _yVel = -0.2f;
        }
        if (!_charController.isGrounded)
        {
            if (_yVel == -0.2f) _yVel = 0f;
            _charController.slopeLimit = 0f;
            _yVel += Physics.gravity.y * Time.deltaTime * _gravityScale;
        }
        if (_jumpTrigger)
        {
            _yVel = _jumpForce;
            _jumpTrigger = false;
        }
        _charController.Move(Vector3.up * _yVel * Time.deltaTime);

        if (moveInput.magnitude > 0)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(moveInput),_rotationSpeed *Time.deltaTime);
        }
    }
}
