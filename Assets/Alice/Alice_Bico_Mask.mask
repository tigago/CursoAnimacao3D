%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Alice_Bico_Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bolsa
    m_Weight: 0
  - m_Path: Corpo
    m_Weight: 0
  - m_Path: Origem
    m_Weight: 0
  - m_Path: Origem/Pelvis
    m_Weight: 0
  - m_Path: Origem/Pelvis/Bolsa_Joint
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Anelar1_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Anelar1_D/Anelar2_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Anelar1_D/Anelar2_D/Anelar3_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Dedao1_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Dedao1_D/Dedao2_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Dedao1_D/Dedao2_D/Dedao3_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Indicador1_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Indicador1_D/Indicador2_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Indicador1_D/Indicador2_D/Indicador3_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Medio1_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Medio1_D/Medio2_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Medio1_D/Medio2_D/Medio3_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Mindinho1_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Mindinho1_D/Mindinho2_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_D/Ombro_D/Cotovelo_D/Antebraco_D/Pulso_D/Mindinho1_D/Mindinho2_D/Mindinho3_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Anelar1_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Anelar1_E/Anelar2_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Anelar1_E/Anelar2_E/Anelar3_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Dedao1_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Dedao1_E/Dedao2_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Dedao1_E/Dedao2_E/Dedao3_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Indicador1_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Indicador1_E/Indicador2_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Indicador1_E/Indicador2_E/Indicador3_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Medio1_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Medio1_E/Medio2_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Medio1_E/Medio2_E/Medio3_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Mindinho1_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Mindinho1_E/Mindinho2_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Clavicula_E/Ombro_E/Cotovelo_E/Antebraco_E/Pulso_E/Mindinho1_E/Mindinho2_E/Mindinho3_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Pescoco
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Pescoco/Cabeca
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Pescoco/Cabeca/Boca
    m_Weight: 1
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Pescoco/Cabeca/Olho_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coluna1/Coluna2/Pescoco/Cabeca/Olho_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coxa_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coxa_D/Joelho_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coxa_D/Joelho_D/Tornozelo_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coxa_D/Joelho_D/Tornozelo_D/Pe_D
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coxa_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coxa_E/Joelho_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coxa_E/Joelho_E/Tornozelo_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Coxa_E/Joelho_E/Tornozelo_E/Pe_E
    m_Weight: 0
  - m_Path: Origem/Pelvis/Rabo
    m_Weight: 0
  - m_Path: Penas_Bracos
    m_Weight: 0
  - m_Path: Penas_Peito
    m_Weight: 0
  - m_Path: Penas_Pernas
    m_Weight: 0
